# ------------------------------------------- 
# FEniCS code  Variational Fracture Mechanics
#################################################################################################################
#                                                                                                               #
# A static solution of gradient damage models for polydisperse polymer networks (incompressible model)          #
# author: Bin Li                                                                                                #
# Email: bl736@cornell.edu                                                                                      #
# date: 10/01/2018                                                                                              #
#                                                                                                               #
#################################################################################################################
# e.g. python3 traction_3D.py --meshsize 100
#################################################################################################################
# References:                                                                                                   #
# [1] Verron, E., & Gros, A. (2017). An equal force theory for network models of soft materials with            #
#     arbitrary molecular weight distribution. Journal of the Mechanics and Physics of Solids, 106, 176-190.    #
# [2] Mao, Y., Talamini, B., & Anand, L. (2017). Rupture of polymers by chain scission.                         #
#     Extreme Mechanics Letters, 13, 17-24.                                                                     #
# [3] Jedynak, R. (2017). "New facts concerning the approximation of the inverse Langevin function".            #
#     Journal of Non-Newtonian Fluid Mechanics. 249: 8-25.                                                      #
#################################################################################################################


# ----------------------------------------------------------------------------
from __future__ import division
from dolfin import *
from mshr import *
from scipy import optimize

import argparse
import math
import os
import shutil
import sympy
import sys
import numpy as np
import matplotlib.pyplot as plt

# ----------------------------------------------------------------------------
# Parameters for DOLFIN and SOLVER 
# ----------------------------------------------------------------------------
set_log_level(LogLevel.WARNING)  # 20, // information of general interest
# set some dolfin specific parameters
# ----------------------------------------------------------------------------
parameters["form_compiler"]["representation"]="uflacs"
parameters["form_compiler"]["optimize"]=True
parameters["form_compiler"]["cpp_optimize"]=True
parameters["form_compiler"]["quadrature_degree"]=4
info(parameters,True)

# set the user parameters
parameters.parse()
userpar = Parameters("user")
userpar.add("E",2.5e3)         # n*E_b stretch modulus
userpar.add("mu",1.25)         # n*k*T Shear modulus 
userpar.add("Gc",1.0e2)        # fracture toughness
userpar.add("Eh",4.)           # number of characteristical Kuhn segments of the Hybrid chain
userpar.add("EN",4.)           # average number of Kuhn segments
userpar.add("K",1.25e6)        # bulk modulus for slightly compressibility
userpar.add("meshsize",100) 
userpar.add("load_min",0.)
userpar.add("load_max",3.6)
userpar.add("load_steps",361)
# Parse command-line options
userpar.parse()

# Material constants
# ----------------------------------------------------------------------------
E       = userpar.E           
mu      = userpar.mu 
K       = userpar.K             
Nh      = userpar.Eh       
EN      = userpar.EN       
Gc      = userpar.Gc    
k_ell   = Constant(1.e-14)  # residual stiffness

# -----------------------------------------------------------------------------
# parameters of the solvers
solver_u_parameters   = {"nonlinear_solver": "snes",
                         "snes_solver": {"linear_solver": "mumps",
                                         "method" : "newtontr",
                                         "line_search": "cp", 
                                         "preconditioner" : "hypre_amg", 
                                         "maximum_iterations": 200,
                                         "absolute_tolerance": 1e-8,
                                         "relative_tolerance": 1e-7,
                                         "solution_tolerance": 1e-7,
                                         "report": True,
                                         "error_on_nonconvergence": False}}                           
# parameters of the PETSc/Tao solver used for the alpha-problem
tao_solver_parameters = {"maximum_iterations": 100,
                         "report": False,
                         "line_search": "more-thuente",
                         "linear_solver": "cg",
                         "preconditioner" : "hypre_amg",
                         "method": "tron",
                         "gradient_absolute_tol": 1e-8,
                         "gradient_relative_tol": 1e-8,
                         "error_on_nonconvergence": True}                       

# Geometry paramaters
L, H, W = 1.0, 0.1, 0.02
N       = userpar["meshsize"]
hsize   = float(L/N)

# Loading Parameters
ut      = 1.0 # reference value for the loading (imposed displacement)

# Numerical parameters of the alternate minimization
maxiteration = 2000
AM_tolerance = 1e-4

modelname = "traction_3D"
meshname  = modelname+"-mesh.xdmf"
simulation_params = "L_%.1f_H_%.1f_W_%.2f_h_%.4f" % (L, H, W, hsize)
savedir   = "output/"+modelname+"/"+simulation_params+"/"

if MPI.rank(MPI.comm_world) == 0:
    if os.path.isdir(savedir):
        shutil.rmtree(savedir)

# Mesh generation 
mesh = BoxMesh(Point(0.0, 0.0, 0.0), Point(L, H, W), N, int(N*H/L), int(N*W/L))
geo_mesh  = XDMFFile(MPI.comm_world, savedir+meshname)
geo_mesh.write(mesh)
"""
#read mesh
mesh = Mesh()
XDMF = XDMFFile(mpi_comm_world(), "traction_3Dbar.xdmf")
XDMF.read(mesh)  
"""
mesh.init()
ndim = mesh.geometry().dim()  # get number of space dimensions
if MPI.rank(MPI.comm_world) == 0:
    print ("the dimension of mesh: {0:2d}".format(ndim))

# regularization paramater
ell     = Constant(5.0*hsize) # damage paramaters

#-----------------------------------------------------------------------------
lmbdab_ = lambda lmbdab:-EN*E*(lmbdab-1.)**2+2.*mu*EN*ln(mu/(mu+E*(lmbdab-1.)*lmbdab)) \
		  + 3.*Gc/8./ell
lmbdabc = optimize.brenth(lmbdab_, 1.0, 2.0)
#lmbdabc = 1.+sqrt(3.*Gc/(8.*E*EN*(5.*mesh.hmin())))
lmbdacc = E*(lmbdabc-1.)*lmbdabc**2*sqrt(Nh)/(mu+E*(lmbdabc-1.)*lmbdabc)
tc      = 2.*lmbdacc*cos(1./3.*acos(-1./lmbdacc**3))-1.

if MPI.rank(MPI.comm_world) == 0:
	print("The critical loading: [{}]".format(tc))

# ----------------------------------------------------------------------------
# Define boundary sets for boundary conditions
# ----------------------------------------------------------------------------
def left_boundary(x, on_boundary):
    return on_boundary and near(x[0], 0.0, 0.1 * hsize)

def right_boundary(x, on_boundary):
    return on_boundary and near(x[0], L, 0.1 * hsize)
"""
## when using "pointwise", the boolean argument on_boundary 
## in SubDomain::inside will always be false    
def left_pinponts1(x, on_boundary):
    return  (x[0]-0.)**2 + (x[1]-H)**2 + (x[2]-0.)**2 < (0.1*mesh.hmax())**2
def right_pinponts1(x, on_boundary):
    return  (x[0]-L)**2 + (x[1]-H)**2 + (x[2]-0.)**2 < (0.1*mesh.hmax())**2
def left_pinponts2(x, on_boundary):
    return  (x[0]-0.)**2 + (x[1]-0.)**2  < (0.1*mesh.hmax())**2
def right_pinponts2(x, on_boundary):
    return  (x[0]-L)**2 + (x[1]-0.)**2  < (0.1*mesh.hmax())**2

for x in mesh.coordinates():
  if (x[0]-0.)**2 + (x[1]-H)**2 + (x[2]-0.)**2 < (0.1*mesh.hmax())**2: 
      print('%s is on x = L' % x)
  if (x[0]-L)**2 + (x[1]-H)**2 + (x[2]-0.)**2 < (0.1*mesh.hmax())**2: 
      print('%s is on x = R' % x)
  if (x[0]-0.)**2 + (x[1]-0.)**2  < (0.1*mesh.hmax())**2: 
      print('%s is on x = L3' % x)
  if (x[0]-L)**2 + (x[1]-0.)**2  < (0.1*mesh.hmax())**2: 
      print('%s is on x = R3' % x)
"""
# ----------------------------------------------------------------------------
# Constitutive functions of the damage model
# ----------------------------------------------------------------------------
# Constitutive functions of the damage model
def w(alpha):
    return alpha

def a(alpha):
    return (1.0-alpha)**2

def b(alpha):
    return (1.0-alpha)**3

# ----------------------------------------------------------------------------
# Variational formulation 
# ----------------------------------------------------------------------------
# Create function space for elasticity + Damage
# Taylor-Hood space for incompressible elasticity
P1      = FiniteElement("Lagrange", mesh.ufl_cell(), 1) 
P2      = VectorElement("Lagrange", mesh.ufl_cell(), 2) 
TH      = MixedElement([P2,P1])
V_u     = FunctionSpace(mesh, TH)

V_alpha = FunctionSpace(mesh, "Lagrange", 1)

# Define the function, test and trial fields
w_p     = Function(V_u)
u_p     = TrialFunction(V_u)
v_q     = TestFunction(V_u)
(u, p)  = split(w_p)
alpha   = Function(V_alpha)
dalpha  = TrialFunction(V_alpha)
beta    = TestFunction(V_alpha)
"""
# --------------------------------------------------------------------
# Dirichlet boundary condition
# --------------------------------------------------------------------
u0 = Expression("0.0", degree=0)
u1 = Expression( "t",  t=0.0, degree=0)
# bc - u (imposed displacement)
bc_u0 = DirichletBC(V_u.sub(0).sub(0), u0, left_boundary)
bc_u1 = DirichletBC(V_u.sub(0).sub(0), u1, right_boundary) 
bc_u2 = DirichletBC(V_u.sub(0).sub(1), u0, left_pinponts2,  method="pointwise") 
bc_u3 = DirichletBC(V_u.sub(0).sub(1), u0, right_pinponts2, method="pointwise") 
bc_u4 = DirichletBC(V_u.sub(0).sub(2), u0, left_pinponts1,  method="pointwise") 
bc_u5 = DirichletBC(V_u.sub(0).sub(2), u0, right_pinponts1, method="pointwise") 
bc_u  = [bc_u0, bc_u1, bc_u2, bc_u3, bc_u4, bc_u5]
"""
u0 = Expression(["0.0","0.0","0.0"], degree=0)
u1 = Expression("t",  t=0.0, degree=0)
# bc - u (imposed displacement)
bc_u0 = DirichletBC(V_u.sub(0), u0, left_boundary)
bc_u1 = DirichletBC(V_u.sub(0).sub(0), u1, right_boundary) 
bc_u = [bc_u0, bc_u1]

# bc - alpha (zero damage)
bc_alpha0 = DirichletBC(V_alpha, 0.0, left_boundary)
bc_alpha1 = DirichletBC(V_alpha, 0.0, right_boundary)
bc_alpha = [bc_alpha0, bc_alpha1]

# --------------------------------------------------------------------
# Define the energy functional of damage problem
# --------------------------------------------------------------------
# Kinematics
d = len(u)
I = Identity(d)             # Identity tensor
F = I + grad(u)             # Deformation gradient
C = F.T*F                   # Right Cauchy-Green tensor

# Invariants of deformation tensors
Ic = tr(C)
J  = det(F)

def bondStretch(lambdac):
  """ compute the Kuhn segement stretch """
  C = mu/E
  DOLFIN_EPS = 1.E-14
  p = (-1. + lambdac-lambdac**2)/3.
  q = (-2.*(1.+lambdac)**3+9.*lambdac*(1.+lambdac) -27.*C*lambdac)/27.
  # bound the argument of 'acos ' both from above and from below  
  r0= (3.*q)/(2.*p)*sqrt(-3./p)
  r1= conditional( ge(r0,  1.-DOLFIN_EPS), 1.-DOLFIN_EPS, r0 )
  r = conditional( le(r0, -1.+DOLFIN_EPS), -1.+DOLFIN_EPS,r1 )

  lambdab = 2.*sqrt(-p/3.)*cos(1./3.*acos(r)) + (1.+lambdac)/3.
  return lambdab

# normalized chain stretch sqrt(Ic/3.0)/sqrt(EN)
lambdac = sqrt(Ic/3.0)/sqrt(Nh) 

# Kuhn segment stretch
lambdab = bondStretch(lambdac) 

def bondEnergy(lambdab, alpha):
    return (a(alpha)+k_ell)*0.5*E*EN*(lambdab-1.)**2 

# strain energy density (eight-chain network average)
def entropyEnergy(lambdac, lambdab, alpha):
    """ eight-chain network average """
    lmbda     = lambdac/lambdab
    # the inverse Langevin function approximated by Pade approximants given by Jedynak, R.
    iLangevin = -0.015+0.0072*lmbda+0.4887*lmbda**2-0.0025*lmbda**3-0.0035*lmbda**4\
                -0.0015*lmbda**5-0.1627*lmbda**6+0.001*lmbda**7+0.0603*lmbda**8 \
                -ln(1.0-lmbda)-0.992*ln(0.985+lmbda)           
    return (a(alpha)+k_ell)*mu*EN*iLangevin 


def bulkEnergy(J, p, alpha):
  return  -(b(alpha)+k_ell)*p*(J-1.0)-1./(2.*K)*p**2 

bond_energy       = bondEnergy(lambdab, alpha)*dx
entropy_energy    = (entropyEnergy(lambdac, lambdab, alpha) - entropyEnergy(1.0/sqrt(Nh), bondStretch(1.0/sqrt(Nh)), 0.0))*dx
bulk_energy       = bulkEnergy(J, p, alpha)*dx
elastic_energy    = bond_energy + entropy_energy + bulk_energy
body_force        = Constant((0., 0., 0.))
external_work     = dot(body_force, u)*dx
elastic_potential = elastic_energy - external_work 

# Compute directional derivative about w_p in the direction of v (Gradient)
F_u = derivative(elastic_potential, w_p, v_q)
# Compute directional derivative about alpha in the direction of dalpha (Hessian)
J_u = derivative(F_u, w_p, u_p)

# Variational problem for the displacement
problem_u = NonlinearVariationalProblem(F_u, w_p, bc_u, J=J_u)
# Set up the solvers                                        
solver_u  = NonlinearVariationalSolver(problem_u)
solver_u.parameters.update(solver_u_parameters)
# info(solver_u.parameters, True)

# --------------------------------------------------------------------
# Define the energy functional of damage problem
# --------------------------------------------------------------------
alpha_0 = interpolate(Expression("0.", degree=0), V_alpha)  # initial (known) alpha
z = sympy.Symbol("z", positive=True)
c_w = float(4 * sympy.integrate(sympy.sqrt(w(z)), (z, 0, 1)))
dissipated_energy = Gc/float(c_w)*(w(alpha)/ell + ell*dot(grad(alpha), grad(alpha)))*dx
damage_functional = elastic_potential + dissipated_energy

# Compute directional derivative about alpha in the direction of beta (Gradient)
E_alpha       = derivative(damage_functional, alpha, beta)
# Compute directional derivative about alpha in the direction of dalpha (Hessian)
E_alpha_alpha = derivative(E_alpha, alpha, dalpha)

# --------------------------------------------------------------------
# Implement the box constraints for damage field
# --------------------------------------------------------------------
# Variational problem for the damage (non-linear to use variational inequality solvers of petsc)
# Define the minimisation problem by using OptimisationProblem class
class DamageProblem(OptimisationProblem):

    def __init__(self):
        OptimisationProblem.__init__(self)
        self.total_energy = damage_functional
        self.Dalpha_total_energy = E_alpha
        self.J_alpha = E_alpha_alpha
        self.alpha = alpha
        self.bc_alpha = bc_alpha

    def f(self, x):
        self.alpha.vector()[:] = x
        return assemble(self.total_energy)

    def F(self, b, x):
        self.alpha.vector()[:] = x
        assemble(self.Dalpha_total_energy, b)
        for bc in self.bc_alpha:
            bc.apply(b)

    def J(self, A, x):
        self.alpha.vector()[:] = x
        assemble(self.J_alpha, A)
        for bc in self.bc_alpha:
            bc.apply(A)

# Set up the solvers                                        
solver_alpha  = PETScTAOSolver()
solver_alpha.parameters.update(tao_solver_parameters)
# info(solver_alpha.parameters,True) # uncomment to see available parameters

#alpha_lb = interpolate(Expression("x[0]<=0.5*L & near(x[1], 0.0, tol) ? 1.0 : 0.0", \
#                                  degree=0, L= L, tol=1.2*cra_w), V_alpha)  # initial (known) alpha
alpha_lb = interpolate(Expression("0.", degree=0), V_alpha)  # lower bound, set to 0
alpha_ub = interpolate(Expression("1.", degree=0), V_alpha)  # upper bound, set to 1

# loading and initialization of vectors to store time datas
load_multipliers  = np.linspace(userpar["load_min"], userpar["load_max"], userpar["load_steps"])
energies          = np.zeros((len(load_multipliers), 8))
iterations        = np.zeros((len(load_multipliers), 2))

# set the saved data file name
(u, p)      = w_p.split()
file_u      = XDMFFile(MPI.comm_world, savedir + "/u.xdmf")
file_u.parameters["rewrite_function_mesh"]     = False
file_u.parameters["flush_output"]              = True
file_p      = XDMFFile(MPI.comm_world, savedir + "/p.xdmf")
file_p.parameters["rewrite_function_mesh"]     = False
file_p.parameters["flush_output"]              = True
file_alpha  = XDMFFile(MPI.comm_world, savedir + "/alpha.xdmf")
file_alpha.parameters["rewrite_function_mesh"] = False
file_alpha.parameters["flush_output"] = True
# write the parameters to file
File(savedir+"/parameters.xml") << userpar

# ----------------------------------------------------------------------------
# Solving at each timestep
# ----------------------------------------------------------------------------
for (i_t, t) in enumerate(load_multipliers):
    u1.t = t * ut
    if MPI.rank(MPI.comm_world) == 0:
        print("\033[1;32m--- Starting of Time step {0:2d}: t = {1:4f} ---\033[1;m".format(i_t, t)) 
    # Alternate Mininimization 
    # Initialization
    iteration = 1
    err_alpha = 1.0
    # Iterations
    while err_alpha > AM_tolerance and iteration < maxiteration:
        # solve elastic problem
        solver_u.solve()
        # solve damage problem with box constraint 
        solver_alpha.solve(DamageProblem(), alpha.vector(), alpha_lb.vector(), alpha_ub.vector())
        # test error
        alpha_error = alpha.vector() - alpha_0.vector()
        err_alpha = alpha_error.norm('linf')
        # monitor the results
        if MPI.rank(MPI.comm_world) == 0:
          print ("AM Iteration: {0:3d},  alpha_error: {1:>14.8f}".format(iteration, err_alpha))
        # update iteration
        alpha_0.assign(alpha)
        iteration = iteration + 1   
    # updating the lower bound to account for the irreversibility
    alpha_lb.vector()[:] = alpha.vector()
    alpha.rename("Damage", "alpha")
    u.rename("Displacement", "u")
    p.rename("Pressure", "p")

    # Dump solution to file 
    file_alpha.write(alpha, t)
    file_u.write(u, t)
    file_p.write(p, t)

    # ----------------------------------------
    # Some post-processing
    # ----------------------------------------
    # Save number of iterations for the time step    
    iterations[i_t] = np.array([t, iteration])

    # Calculate the energies
    elastic_energy_value = assemble(elastic_energy)
    surface_energy_value = assemble(dissipated_energy)
    bond_energy_value    = assemble(bond_energy)
    entropy_energy_value = assemble(entropy_energy)
    bulk_energy_value    = assemble(bulk_energy)
    volume_ratio         = assemble(J/(L*H*W)*dx)

    energies[i_t] = np.array([t, elastic_energy_value, surface_energy_value, elastic_energy_value+surface_energy_value,\
    					      bond_energy_value, entropy_energy_value, bulk_energy_value, volume_ratio])

    if MPI.rank(MPI.comm_world) == 0:
        print("\nEnd of timestep {0:3d} with load multiplier {1:4f}".format(i_t, t))
        print("\nElastic and Surface Energies: [{0:6f},{1:6f}]".format(elastic_energy_value, surface_energy_value))
        print("\nElastic and Surface Energies: [{},{}]".format(elastic_energy_value, surface_energy_value))
        print("-----------------------------------------")
        # Save some global quantities as a function of the time
        np.savetxt(savedir + '/energies.txt', energies)
        np.savetxt(savedir + '/iterations.txt', iterations)
# ----------------------------------------------------------------------------

# Plot energy and stresses
if MPI.rank(MPI.comm_world) == 0:
    p1, = plt.plot(energies[slice(None), 0], energies[slice(None), 1])
    p2, = plt.plot(energies[slice(None), 0], energies[slice(None), 2])
    p3, = plt.plot(energies[slice(None), 0], energies[slice(None), 3])
    plt.legend([p1, p2, p3], ["Elastic", "Dissipated", "Total"], loc="best", frameon=False)
    plt.xlabel('Displacement')
    plt.ylabel('Energies')
    plt.savefig(savedir + '/energies.pdf', transparent=True)
    plt.close()
    p4, = plt.plot(energies[slice(None), 0], energies[slice(None), 4])
    p5, = plt.plot(energies[slice(None), 0], energies[slice(None), 5])
    p6, = plt.plot(energies[slice(None), 0], energies[slice(None), 6])
    plt.legend([p4, p5, p6], ["Bond", "Entropy", "Bulk"], loc="best", frameon=False)
    plt.xlabel('Displacement')
    plt.ylabel('Energies')
    plt.savefig(savedir + '/other-energies.pdf', transparent=True)
    plt.close()
