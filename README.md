# Gradient-Damage-Polymer #
A FEniCS Project-based code for simulating damage of polymer networks.

# Description #
traction_3D.py is an open-source code that provides finite element based
implementation of gradient damage models, which are used for simulating damage 
of **incompressible polydisperse** polymer networks. 
It is based on [FEniCS Project](http://fenicsproject.org).

# Citing #
Please cite the [following work](https://doi.org/10.1016/j.ijsolstr.2019.08.012) if you find this code useful for your work:

    @article{li2019gradient,
	    Title = {A variational phase-field model for brittle fracture in polydisperse elastomer networks},
        Author = {Li, Bin and Bouklas, Nikolaos},
		Journal = {in Press},
        Volume = {182-183},
        Pages = {193-204},
        Year = {2020}}
       
along with the appropriate general [FEniCS citations](http://fenicsproject.org/citing).

# Getting started #
1. Install FEniCS by following the instructions at <http://fenicsproject.org/download>. 
   We recommend using Docker to install FEniCS.
   
2. Then, clone this repository using the command:

    ```git clone https://bitbucket.org/bin-mech/gradientDamagePolydisperse.git ``` 

3. Running the code:
   ``` python traction_3D.py```  
   
    In parallel:
   ``` mpiexec -np 8 python traction_3D.py``` 
   
4. For building the latest stable version of FEniCS with PETSc and SLEPc support on High Performance Computing platform,
   we refer to [fenics-mesu-hpc](https://bitbucket.org/bin-mech/fenics-mesu-hpc) for details.

# Contributing #
We are always looking for contributions and help. If you have ideas, nice applications
or code contributions then we would be happy to help you get them included. We ask you
to follow the [FEniCS Project git workflow](https://bitbucket.org/fenics-project/dolfin/wiki/Git%20cookbook%20for%20FEniCS%20developers).

# Issues and Support #
For support or questions please email 
**Bin Li** at <bl736@cornell.edu>   

# Author #
| Bin LI, Sibley School of Mechanical Engineering, Ithaca, USA

# License #
Gradient-damage-polymer is free software: you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with gradient-damage.  If not, see http://www.gnu.org/licenses/.

# Notes #
HDF5File stores the output in a special way allowing for loading of data into dolfin,
but incompatible with any viewer.

* To store data for viewing, use **.xdmf** format

* To store data for reloading, use **.HDF5** format

* To store data for viewing and reloading, use **.xdmf** and **.HDF5** format

We use **gmsh** for unstructured mesh generation. Make sure that **gmsh** is in your system **PATH**.
For **multi-material** , you could assign indicators for **subdomains** and **boundaries** directly in 
the ``.geo`` file, for instance :
``Physical Line (%) = {%};``
``Physical Surface (%) = {%};``.
